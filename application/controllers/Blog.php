<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->model('Blogs_model');
    }
	public function index()
	{
		$data=array();
		$data['title']="Blogs";

		$user=$this->session->userdata();
		if(isset($user['id']) && !empty($user['id']))
		{
			$data['islogin']=$user['id'];
		}
		else 
		{
			$data['islogin']=0;
		}
		$this->load->view('blog/blog_list',$data);
       
	}
	public function add()
	{
		$data=array();
		$data['title']="Blog add";
		$user=$this->session->userdata();
		if(isset($user['id']) && !empty($user['id']))
		{
			if($this->input->post('signup') && $this->input->post('signup')=="Save")
			{
				$this->form_validation->set_rules('name','name','trim|required');
				$this->form_validation->set_rules('description','description','trim|required');
				$this->form_validation->set_rules('tag[]','Tag','trim|required');
				if ($this->form_validation->run() == FALSE)
				{
					$this->session->set_flashdata("error","All field are required");
				}
				else
				{
					$image="";
					if(isset($_FILES['image']) && !empty($_FILES['image']['name'])){
						$path=$this->config->item('blog_uploadpath');
						$image=$this->file_upload($_FILES['image'],$path);
					}
					$insert=array();
					$insert['name']=$this->input->post('name');
					$insert['description']=$this->input->post('description');
					$insert['created_at']=date('Y-m-d H:i:s');
					$insert['image']=$image;
					$insert['user_id']=$user['id'];

					$id=$this->Common_model->data_insert('tbl_blog',$insert);
					if(!empty($id))
					{
						if(isset($_POST['tag']) && !empty($_POST['tag']) && count($_POST['tag']) > 0)
						{
							foreach($_POST['tag'] as $row)
							{
								$tagaaray=array();
								$tagaaray['blog_id']=$id;
								$tagaaray['tag']=$row;
								$tagaaray['user_id']=$user['id'];
								$tagaaray['created_at']=date('Y-m-d H:i:s');
								$this->Common_model->data_insert('tbl_tags',$tagaaray);
								
								
							}
						}
					}
					
					

					
					
				}
			}
			$this->load->view('blog/add',$data);
		}
		else 
		{
			redirect('user/login');
		}
	}
	private function file_upload($arr, $path)
    {
        set_time_limit(0);
        if ($arr['error'] == 0)
        {
            $temp = explode(".", $arr["name"]);
            $file_name = uniqid() . time() . '.' . end($temp);
            $file_path = $path . $file_name;
            if (move_uploaded_file($arr["tmp_name"], $file_path) > 0)
            {
                $ret = $file_name;
            }
            else
            {
                $ret = "";
            }
        }
        return $ret;
    }
}
