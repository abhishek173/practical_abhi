<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
    }
	
	public function index()
	{
        $data=array();
        $data['title']="Sign in";
        if($this->input->post('signup') && $this->input->post('signup')=="Register")
        {
            
            $this->form_validation->set_rules('username','Username','trim|required');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email');
            $this->form_validation->set_rules('password','Password','trim|required');
            $this->form_validation->set_rules('password_confirmation','password_confirmation','trim|required|matches[password]');
            if ($this->form_validation->run() == FALSE)
            {
                $this->load->view('users/signup',$data); 
            }
            else
            {
                $insertarray=array();
                $insertarray['username']=$this->input->post('username');
                $insertarray['email']=$this->input->post('email');
                $insertarray['password']=md5($this->input->post('password'));
                $insertarray['created_at']=date("Y-m-d H:i:s");
                $insertid= $this->Common_model->data_insert('tbl_users',$insertarray);
                if(!empty($insertid))
                {
                    $this->session->set_flashdata("message","signup successful");
                    redirect('blog');
                }
                else
                {
                    $this->session->set_flashdata("error","signup successful");
                    redirect('user');
                }
                exit;
                

                
            }

        }
        
		$this->load->view('users/signup',$data);
	}
    public function login(){
        $data=array();
        $data['title']="Login";
        if($this->input->post('login') && $this->input->post('login')=="login")
        {
            $this->form_validation->set_rules('email','Email','trim|required|valid_email');
            $this->form_validation->set_rules('password','Password','trim|required');
            if ($this->form_validation->run() == FALSE)
            {
                $this->load->view('users/signup',$data); 
            }
            else
            {
                $email=$this->input->post('email');
                $password=md5($this->input->post('password'));
                $where=array(
                    'email'=>$email,
                    'password'=>$password
                );
                $this->db->select('*');
                $this->db->from('tbl_users');
                $this->db->where($where);
                $res=$this->db->get()->row();
                if(isset($res) && !empty($res) && count($res) > 0)
                {
                    $sessiondata=array(
                        'id'=>$res->id,
                        'username'=>$res->username,
                        'email'=>$res->email,
                    );
                    $this->session->set_userdata($sessiondata);
                    $this->session->set_flashdata("message","signup successful");
                    redirect('blog');
                }
                else
                {
                    $this->session->set_flashdata("error","Email and password not match");
                    redirect('user/login');
                }
                
                
            }
        }
        $this->load->view('users/login',$data);
    }
    
}
