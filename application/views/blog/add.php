<?php $this->load->view('common/header'); ?>
<div class="container">
        <div class="row centered-form">
        <div class="col-xs-12 col-sm-12 col-md-12">
        	<div class="panel panel-default">
        		<div class="panel-heading">
			    		<h3 class="panel-title"><?php echo $title; ?></h3>
                        <?php echo validation_errors(); 
                            echo $this->session->flashdata('error');
                            ?>
			 			</div>
			 			<div class="panel-body">
			    		<form role="form" id="form" method="post" enctype='multipart/form-data'> 
			    			<div class="row">
			    				<div class="col-xs-12 col-sm-12 col-md-12">
			    					<div class="form-group">
			                            <input type="text" name="name" id="name" class="form-control input-sm" placeholder="Blog name">
			    					</div>
			    				</div>
			    		    </div>
                            <div class="row">
			    				<div class="col-xs-12 col-sm-12 col-md-12">
			    					<div class="form-group">
                                        <textarea name="description" class="form-control input-sm" placeholder="Blog Description"></textarea>
			                        </div>
			    				</div>
			    		    </div>
                            <div class="row">
			    				<div class="col-xs-12 col-sm-12 col-md-12">
			    					<div class="form-group">
			                            <input type="text" name="tag[]" id="tag" class="form-control input-sm" placeholder="Tag">
                                        <div class="moretag">

                                        </div>
			    					</div>
                                    <a href="javascript:void(0);" class="addrow">add more</a>
                                </div>
			    		    </div>
			    			<div class="form-group">
			    				<input type="file" name="image" id="image" class="form-control input-sm" placeholder="Email Address">
			    			</div>
                            <input type="submit" name="signup" value="Save" class="btn btn-info btn-block">
                             
			    		</form>
			    	</div>
	    		</div>
    		</div>
    	</div>
    </div>
<?php $this->load->view('common/footer'); ?>
<script>
$('.addrow').click(function(){
    var html='<input type="text"  name="tag[]" id="tag" class="form-control input-sm" placeholder="Tag"><a href="javascript:void(0)"  class="remove">Remove</a>';
    $('.moretag').append(html);
});

$('body').on('click', '.remove', function(){
    $(this).prev('input').remove();
    $(this).remove();
    });
$(function() {

    $.validator.addMethod('filesize', function (value, element,param) {
   
   var size=element.files[0].size;
  
   size=size/1024;
   size=Math.round(size);
   return this.optional(element) || size <=param ;
   
 }, 'File size must be less than {0}');
  
  $("#form").validate({
    // Specify validation rules
    rules: {
        name: 
        {
            required: true,
            maxlength: 255,
        },
        description:
        {
            required: true,
            maxlength: 65535,
        },
        "tag[]":
        {
            required: true,
        },
        image:
        {
            required: true,
            extension: "jpg,jpeg,png",
            filesize: 1000
        }

    },
    messages: {
       name: 
        {
            required: "Blog title is required",
            maxlength: "Please enter no more than 255 characters."
        },
        description:
        {
            required: "Please enter blog description",
            maxlength:"Please enter no more than 65535 characters.",
        },
        "tag[]":
        {
            required: "Please enter at least 1 tag",
        },
        image:
        {
            required: "Please Select image",
            filesize: 100
        }
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});
</script>