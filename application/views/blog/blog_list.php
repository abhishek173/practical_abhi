<?php $this->load->view('common/header'); ?>
<div class="container">
<?php if($islogin > 0 && !empty($islogin))
{?>
    <div class="col-xs-12 col-sm-12 col-md-12">
    
        <a class="btn btn-primary pull-left" href="<?php echo base_url();?>blog/add">Create Blog</a>
        <a class="btn btn-danger pull-right" href="<?php echo base_url();?>blog/add">Logout</a>
    </div>
<?php } ?>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <?php echo validation_errors(); 
        echo $this->session->flashdata('error'); ?>

        
    </div>
    
<?php $this->load->view('common/footer'); ?>
<script>
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("#form").validate({
    // Specify validation rules
    rules: {
      
      
      email: {
        required: true,
        email: true,
        
      },
      password: {
        required: true,
        
      },
      
    },
    // Specify validation error messages
    messages: {
        password: {
            required: "Please provide a password",
        },
        email: "Please enter a valid email address",
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});
</script>