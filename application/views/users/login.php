<?php $this->load->view('common/header'); ?>
<div class="container">
        <div class="row centered-form">
        <div class="col-xs-12 col-sm-12 col-md-12">
        <?php echo validation_errors(); 
        echo $this->session->flashdata('error');
        ?>

        	<div class="panel panel-default">
        		<div class="panel-heading">
			    		<h3 class="panel-title"><?php echo $title; ?></h3>
			 			</div>
			 			<div class="panel-body">
			    		<form role="form" id="form" method="post"> 
			    			
                        <div class="col-xs-12 col-sm-12 col-md-12">
			    			<div class="form-group">
			    				<input type="email" name="email" id="email" class="form-control input-sm" placeholder="Email Address">
			    			</div>
                        </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password">
                                </div>
                            </div>
			    		    <input type="submit" name="login" value="login" class="btn btn-info btn-block">
                            <div class="col-xs-12 col-sm-12 col-md-12">
			    					<a href="<?php echo base_url();?>/user">Register here</a>
			    			</div>   
			    		</form>
			    	</div>
	    		</div>
    		</div>
    	</div>
    </div>
<?php $this->load->view('common/footer'); ?>
<script>
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("#form").validate({
    // Specify validation rules
    rules: {
      
      
      email: {
        required: true,
        email: true,
        
      },
      password: {
        required: true,
        
      },
      
    },
    // Specify validation error messages
    messages: {
        password: {
            required: "Please provide a password",
        },
        email: "Please enter a valid email address",
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});
</script>