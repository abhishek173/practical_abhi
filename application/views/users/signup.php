<?php $this->load->view('common/header'); ?>
<div class="container">
        <div class="row centered-form">
        <div class="col-xs-12 col-sm-12 col-md-12">
        	<div class="panel panel-default">
        		<div class="panel-heading">
			    		<h3 class="panel-title"><?php echo $title; ?></h3>
			 			</div>
			 			<div class="panel-body">
			    		<form role="form" id="form" method="post"> 
			    			<div class="row">
			    				<div class="col-xs-12 col-sm-12 col-md-12">
			    					<div class="form-group">
			                    <input type="text" name="username" id="username" class="form-control input-sm" placeholder="User Name">
			    					</div>
			    				</div>
			    		    </div>

			    			<div class="form-group">
			    				<input type="email" name="email" id="email" class="form-control input-sm" placeholder="Email Address">
			    			</div>

			    			<div class="row">
			    				<div class="col-xs-12 col-sm-12 col-md-12">
			    					<div class="form-group">
			    						<input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password">
			    					</div>
			    				</div>
			    				<div class="col-xs-12 col-sm-12 col-md-12">
			    					<div class="form-group">
			    						<input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm Password">
			    					</div>
			    				</div>
			    			</div>
			    			
			    			<input type="submit" name="signup" value="Register" class="btn btn-info btn-block">
                            <div class="col-xs-12 col-sm-12 col-md-12">
			    					<a class="btn btn-primary" href="<?php echo base_url();?>/user/login">Login here</a>
			    			</div>   
			    		</form>
			    	</div>
	    		</div>
    		</div>
    	</div>
    </div>
<?php $this->load->view('common/footer'); ?>
<script>
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("#form").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      username: {required: true},
      email: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true,
        
      },
      password: {
        required: true,
        minlength: 5
      },
      password_confirmation : {
        minlength : 5,
        equalTo : "#password"
        }
    },
    // Specify validation error messages
    messages: {
        username: "Please enter username",
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      email: "Please enter a valid email address",
      password_confirmation:{
        equalTo:"Password and confrim password should be same"
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});
</script>